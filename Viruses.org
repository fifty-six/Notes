* Viruses
A bit of DNA/RNA surounded by a caspid (protein coat cell). They can have a embrane called a viral envelope or a membrane envelope, it comes from the host cell. Has additional proteins on the surface called Ligands.
** Parts
*** Capsids
Protein coat or shell
*** Membrane Envelope/Viral Envelope
Comes from the host cell.
*** Bacteriaphage
Virus which infects bacteria
*** Ligands
Proteins that stick out of the surface of the virus. They act as the key to recognize the cell to be infected and invade it.
- Attaches to the receptors of the cell it infects
** Reproduction
*** Basic interpretation
- Virus enters
  - Virus makes cell make Viral DNA copies
  - Virus makes mRNA which makes cell make capsid proteins
    - Virus is assembled/created
*** Lytic
- Attachment
- Viral DNA enters, Host DNA degrades     (Entry)
- Synthesis of viral genomes and proteins (Replication/Synthesis)
- Assembly
- Release (B U R S T    E X P L O S I O N)
*** Lysogenic
- Attachment/Binding
- Enter
- Integration with host DNA
- Cell Replication
- Repeat
- Stress
- GOTO [[Lytic]] (Replication step)
