#+TITLE: Protein Synthesis + DNA Structure

* Scientests
(ahhh)
** Marshall Nirenberg and J. Heinrich Matthei
Laid groundowrk for cracking the genetic code
1. Found a cellular enzyme usable for synthetic RNA
   - first synthetic rna was only uracil
   - all uracil made all phenylalanine (UUU code discovered)
2. found about other three at a time codes
3. figured out that mRNA codons code amino acids

*** Features
1. Genetic code is degenerate. (Most amino acids have more than one codon)
   Degeneracy = Redundancy
2. Genetic code is unambiguous
   Each code only has one meaning
3. Code has start/stop signals
   - One start scene
   - Three stop signals

* DNA Coding
** Universal
Universal across organisms
except for mitochondria, chloroplasts, and some archaebacteria
- This provides evidence for all living organisms sharing a common evolutionary heritage.
- Due to this, it is possible to transfer genes between animals

** Triplet Code
DNA is coded in a triplet code which means each [[Codon]] is composed of 3 nucleotides
** Codons
3 nucleotides which together correspond to a protein (different nucelotides => different proteins)
** Introns
Noncoding regions of genes
** Exons
Coding regions of genes

* Vocab
** Gene
A segment of DNA
** Trait
Expressed [[Gene]] (which comes from the protein made by the gene)

* Central Dogma of Genetics
DNA -> (m)RNA -> Protein (-> Trait [Textbook but not video])

* RNA Types (Actors)
** mRNA
Copies DNA instructions, carries to ribosomes in cytoplasm
- *is the dna copy* (but rna ofc)
** tRNA
carries amino acids to ribosomes and [[mRNA]]
- carries the amino acid to a sequence on the mRNA strand so it drops of all the amino acids in the right order 
** rRNA
composes the ribosome

* Transcription
*DNA -> mRNA*
- uracil replaces thymine
- ribose over deoxibose sugar
** Vocab
*** Codons
3 nucleotides on a mRNA
- makes 1 amino acid
- reading frame if start??
- redundant sometimes
*** Anti-Codon
- Codon complement
*** translation happens based on a "Genetic Code"
- Universe to all organisms
- Works like a "language" translator RNA nucleotides -> proteins amino acids
** How it works
*** Video
1. Chemical signal turns on a gene
   1. Chemical signals bind to region
   2. Attracts an RMA polymerase
   3. Unwinds DNA
2. Strand made
   1. Polymerase pairs complementary bases.
      - (The other strand hangs out)
3. See steps.
   1. RNA peels off and other DNA strand rebinds
   2. mRNA leaves nucleus
   3. mRNA travels to cytoplasm
- DNA rebinds, (m)RNA peels off
*** Textbook
**** Initiation
Promoter binds
**** Elongation
RNA poly reads DNA
**** Termination
RNA poly stops at DNA stop sequence
- mRNA now called an mRNA Transcript
**** Processing (not really part of transcription)
pre-mRNA -> mRNA
done by splicosomes mostly (which uses a ribozome to cut and remove [[Introns]])
***** Cap
Modified Guanine nucleotide that tells the riobosmes where to attach when translation begins
***** Poly-A Tail
stuff w/out cap basically, the remains of the list.
***** Introns
Non-protein-coding regions
- Helps to choose which exons go into a cell
- help regulate mRNA translation
- encourages crossing over
***** Exons
Protein-coding regions
** Why tho
- DNA cannot leave nucleus and proteins are made in the cytoplasm
- mRNA serves as "messenger" RNA and carries the instructions to ribosomes in cytoplasm
- basically, *info leaves*, but *the DNA stays safe*
  - from enzymes in cytoplasm

* Translation
RNA -> Protein

- There are fewer tRNAs than codons because they can bind w/ multiple sometimes
- Ribosome stuff
  - there are 2 subunits in a ribosome
  - they have a binding site for mRNA and 3 sites for tRNA
    - E - exit (leaving a ribosome)
    - P - peptide
    - A - amino acid (next amino acid entrance)

** Textbook
*** Initiation
Brings translation components together.
- Initiation Factors (Proteins needed to make ribosomal subunits)
- mRNA
- initiator tRNA
- large ribosomal subunit
*** Elongation
Polypeptide increases in length one amino acid at a time
1. verify codon/amino acid
2. transfers the growing peptide and creates a peptide bond

3. Translocation
   Ribosome moves forward and the empty tRNA exits
   - Empty because its amino acid got transfered onto polypeptide
*** Termination
1. Ribosome comes to a stop codon.
2. Release factor binds.
3. Release factor hydrolyzes the bond between the last tRNA and the polypeptide
   - The protein is freee
4. The ribosomal subunits dissociate
** Video
1. mRNA attaches to a ribosome
2. matching tRNA binds to mRNA start codon
3. matching tRNA binds
4. *peptide bonds* between amino acids
   1. probably folds too
5. "empty" tRNA falls off
6. tRNA moves in, each bringing amino acids until empty falls off
7. CONTINUES until stop codon

** Anticodon
The complement of a codon. What the tRNA has so that it binds.
